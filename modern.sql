-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2020 at 02:07 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `modern`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_admin`
--

CREATE TABLE `mst_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `remember_token` varchar(256) NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1 for activate, 2 for deactivate',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_admin`
--

INSERT INTO `mst_admin` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@example.com', '$2y$10$JLvf0A10OCZxFsXH2KolFepwEPIn7x0ayTv8Gg2UDcwIB.8x5nVKC', 'aT59e7mpRilDxJsukhqUE3LMdAn13TRKmP8bcTlyycT9VaVVks1dMSBs3ira', '1', '2019-10-01 00:00:00', '2019-10-01 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_cat`
--

CREATE TABLE `mst_cat` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(20) NOT NULL,
  `cat_img` text NOT NULL,
  `cat_banner` text NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1 COMMENT '0 = inactive and 1 = active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_cat`
--

INSERT INTO `mst_cat` (`id`, `cat_name`, `cat_img`, `cat_banner`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', '[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1589889972_Jack-Jones-Beige-Round-Neck-SDL693531765-5-52974.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1589889993_Jack-Jones-Beige-Round-Neck-SDL693531765-4-46b29.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1589889994_Jack-Jones-Beige-Round-Neck-SDL693531765-3-1ac20.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1589889995_Jack-Jones-Beige-Round-Neck-SDL693531765-2-66430.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1589889996_Jack-Jones-Beige-Round-Neck-SDL693531765-1-6414a.jpg\"]', '[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889997_Jack-Jones-White-Round-Neck-SDL697586396-5-f5acc.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889998_Jack-Jones-White-Round-Neck-SDL697586396-4-0c562.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889999_Jack-Jones-White-Round-Neck-SDL697586396-3-e118e.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589890000_Jack-Jones-White-Round-Neck-SDL697586396-2-a9e3c.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589890002_Jack-Jones-White-Round-Neck-SDL697586396-1-97956.jpg\"]', 1, 1, 1, '2020-05-19 12:06:43', '2020-05-19 12:06:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_child_cat`
--

CREATE TABLE `mst_child_cat` (
  `id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `child_cat_name` varchar(20) NOT NULL,
  `child_cat_img` text NOT NULL,
  `child_cat_banner` text NOT NULL,
  `is_active` int(1) NOT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sub_cat`
--

CREATE TABLE `mst_sub_cat` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_cat_name` varchar(20) NOT NULL,
  `sub_cat_img` text NOT NULL,
  `sub_cat_banner` text NOT NULL,
  `is_active` int(1) NOT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sub_child_cat`
--

CREATE TABLE `mst_sub_child_cat` (
  `id` int(11) NOT NULL,
  `child_cat_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_child_cat_name` varchar(20) NOT NULL,
  `sub_child_cat_img` text NOT NULL,
  `sub_child_cat_banner` text NOT NULL,
  `is_active` int(1) NOT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_admin`
--
ALTER TABLE `mst_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_cat`
--
ALTER TABLE `mst_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_child_cat`
--
ALTER TABLE `mst_child_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sub_cat`
--
ALTER TABLE `mst_sub_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sub_child_cat`
--
ALTER TABLE `mst_sub_child_cat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_admin`
--
ALTER TABLE `mst_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_cat`
--
ALTER TABLE `mst_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_child_cat`
--
ALTER TABLE `mst_child_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_sub_cat`
--
ALTER TABLE `mst_sub_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_sub_child_cat`
--
ALTER TABLE `mst_sub_child_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
