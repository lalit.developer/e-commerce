<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-transparent footer-light navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 container center-layout"><span class="float-md-left d-block d-md-inline-block">Powered by: <a class="text-bold-800 grey darken-2" href="http://cabbagesoft.com/" target="_blank">Cabbagesoft Technologies</a></span><span class="float-md-right d-none d-lg-block">{{$SITE_NAME}} 1.0<span id="scroll-top"></span></span></p>
</footer>
<!-- END: Footer-->