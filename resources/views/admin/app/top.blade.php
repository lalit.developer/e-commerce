<base href="{{ url('/') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
<meta name="author" content="PIXINVENT">
<title>@yield('title', $SITE_NAME) :: {{ $SITE_NAME }}</title>
<link rel="apple-touch-icon" href="{{$ADMIN_THEME_PATH}}/app-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon" href="{{$ADMIN_THEME_PATH}}/app-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/weather-icons/climacons.min.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/fonts/meteocons/style.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/charts/morris.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/charts/chartist.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/colors.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/components.css">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/fonts/simple-line-icons/style.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/pages/dashboard-ecommerce.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/pages/gallery.css">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/animate.min.css">
<link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/noty/noty.css">
<link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/ladda/ladda.min.css">
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/assets/css/style.css">
<!-- END: Custom CSS-->

<!-- BEGIN: Vendor JS-->
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->