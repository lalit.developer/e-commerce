<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Commong Keys
	|--------------------------------------------------------------------------
	*/

	'home' => 'Home',
	'list' => 'List',
	'add' => 'Add',
	'edit' => 'Edit',
	'delete' => 'Delete',

	'actions' => 'Actions',
	'activate' => 'Activate',
	'inactivate' => 'Inactivate',

	'search' => 'Search',
	'reset' => 'Reset',

	'update' => 'Update',
	'create' => 'Create',
	'cancel' => 'Cancel',

	'active' => 'Active',
	'inactive' => 'Inactive',

	'backToList' => 'Back To List',

	//	Header
	'myProfile' => 'My Profile',
	'logOut' => 'Log Out',

	//	Footer
	'footer' => '&copy; Modern',

];
