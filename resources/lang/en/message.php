<?php

return [

    /*
    |--------------------------------------------------------------------------
    |   Section Confirmation Messages
    |--------------------------------------------------------------------------
    */
    
    
    'keyExist'          => ':section Already Exist Please Use Different.',
    
    'detailAdded'       => ':section details has been successfully inserted.',
    'detailUpdated'     => ':section details has been successfully updated.',    
    'detailDeleted'     => ':section details has been successfully deleted.',
    'somethingWrong'    => 'Something Went Wrong',
    'passwordNotMatch'  => 'Password and confirm password did not match.',
    'emailAndPassword'  => 'Email Address and Password does not match.',
    'checkEmail'        => 'Please check your email for reset your password.',
    'emailNotExist'     => 'Your email address doesn’t exist in our system.',
    'requestExpired'    => 'Your reset password request has expired.',
    'reseAccount'       => 'Your account password has been reset successfully.',
    'adminNotExist'     => 'Admin dose not exist.',
    

    'store'             => ':section has been added.',
    'update'            => ':section has been updated.',
    'statusActivate'      => 'Status of :section has been changed to Activate.',
    'statusDeactivate'    => 'Status of :section has been changed to Deactivate.',
    'statusEnabled'      => 'Status of :section has been changed to Enabled.',
    'statusDisabled'    => 'Status of :section has been changed to Disabled.',
    'delete'            => ':section has been deleted.',
    'noRecords'         => 'No Records Found.',
    'sitemap'           => 'Sitemap has been generate successfully.',
];
