<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function() {
	return redirect('admin/login');
});

/*
|--------------------------------------------------------------------------
|		Get PHP Information
|--------------------------------------------------------------------------
*/

Route::get('/get-phpinfo', function () {
  phpinfo();
  die();
});

/*
|--------------------------------------------------------------------------
|		Authentication Routes
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'admin' ], function () {
	Route::get('/', 'Admin\Login@getLogin');				//	Show Login Form
	Route::get('/login', 'Admin\Login@getLogin');			//	Show Login Form
	Route::post('/login', 'Admin\Login@postLogin');			//	Check Login
	Route::get('/logout', 'Admin\Login@getLogout');			//	LogOut
});

/*
|--------------------------------------------------------------------------
|		Dashboard
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'admin/dashboard', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Dashboard@index');
});

/*
|--------------------------------------------------------------------------
|		Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\Category@index');
	Route::post('/','Admin\Category\Category@index');
	Route::get('/add','Admin\Category\Category@Add');
	Route::get('/edit/{id}','Admin\Category\Category@Edit');
	Route::get('/view/{id}','Admin\Category\Category@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\Category@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\Category@Action');
	Route::get('/{id}/{status}/status','Admin\Category\Category@status');
});


/*
|--------------------------------------------------------------------------
|		Sub-category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/sub-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\SubCategory@index');
	Route::post('/','Admin\Category\SubCategory@index');
	Route::get('/add','Admin\Category\SubCategory@Add');
	Route::get('/edit/{id}','Admin\Category\SubCategory@Edit');
	Route::get('/view/{id}','Admin\Category\SubCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\SubCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\SubCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\SubCategory@status');
});

/*
|--------------------------------------------------------------------------
|		Child Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/child-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\ChildCategory@index');
	Route::post('/','Admin\Category\ChildCategory@index');
	Route::get('/add','Admin\Category\ChildCategory@Add');
	Route::get('/edit/{id}','Admin\Category\ChildCategory@Edit');
	Route::get('/view/{id}','Admin\Category\ChildCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\ChildCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\ChildCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\ChildCategory@status');
	Route::post('sub-category', 'Admin\Category\ChildCategory@subCategory');
});

/*
|--------------------------------------------------------------------------
|		Sub Child Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/sub-child-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\SubChildCategory@index');
	Route::post('/','Admin\Category\SubChildCategory@index');
	Route::get('/add','Admin\Category\SubChildCategory@Add');
	Route::get('/edit/{id}','Admin\Category\SubChildCategory@Edit');
	Route::get('/view/{id}','Admin\Category\SubChildCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\SubChildCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\SubChildCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\SubChildCategory@status');
	Route::post('child-category', 'Admin\Category\SubChildCategory@childCategory');
});