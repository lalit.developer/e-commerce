<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {

	protected $table = 'mst_admin';

	public $timestamps = true;

	protected $fillable = [
		'id',
		'name',
		'email',
		'password',
		'remember_token',
		'status',
	];

	protected $hidden = [
		'password',
		'remember_token',
	];
}
