<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class SubChildCategory extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_sub_child_cat';

	public $timestamps = true;

	protected $fillable = [
		'child_cat_id',
		'sub_cat_id',
		'cat_id',
		'sub_child_cat_name',
		'sub_child_cat_img',
		'sub_child_cat_banner',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	protected $casts = ['sub_child_cat_img' => 'array','sub_child_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','id','cat_id');
	}

	public function subcategory()
	{
		return $this->hasOne('App\Models\Category\SubCategory','id','sub_cat_id');
	}

	public function childcategory()
	{
		return $this->hasOne('App\Models\Category\ChildCategory','id','child_cat_id');
	}
}