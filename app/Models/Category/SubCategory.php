<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class SubCategory extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_sub_cat';

	public $timestamps = true;

	protected $fillable = [
		'cat_id',
		'sub_cat_name',
		'sub_cat_img',
		'sub_cat_banner',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	protected $casts = ['sub_cat_img' => 'array','sub_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','id','cat_id');
	}
}