<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class Category extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_cat';

	public $timestamps = true;

	protected $fillable = [
		'cat_name',
		'cat_img',
		'cat_banner',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	protected $casts = ['cat_img' => 'array','cat_banner' => 'array'];

	public function sub_category_active()
    {
        return $this->hasMany('App\Models\Category\SubCategory','category_id','id')->where('is_active','1');
    }

    public function sub_category()
    {
        return $this->hasMany('App\Models\Category\SubCategory','category_id','id');
    }
}