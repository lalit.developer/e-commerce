<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class ChildCategory extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_child_cat';

	public $timestamps = true;

	protected $fillable = [
		'sub_cat_id',
		'cat_id',
		'child_cat_name',
		'child_cat_img',
		'child_cat_banner',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	protected $casts = ['child_cat_img' => 'array','child_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','id','cat_id');
	}

	public function subcategory()
	{
		return $this->hasOne('App\Models\Category\SubCategory','id','sub_cat_id');
	}
}