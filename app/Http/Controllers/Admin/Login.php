<?php
namespace App\Http\Controllers\Admin;
use Request, Auth, Lang;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Admin\LoginRequest;

class Login extends Controller {

	protected function getLogin() {
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;
		return view('admin/login');
	}

	protected function postLogin(LoginRequest $request) {
		$remember_me = $request->only('remember') ? true : false;
		$auth = auth()->guard('admin');
		if ($auth->attempt($request->only('email', 'password'), $remember_me)):
			return redirect('admin/dashboard');
		else:
			return redirect('admin/login')->with( 'warning', Lang::get('message.emailAndPassword') ); 
		endif;	
	}
	
	protected function getLogout() {
		$auth = auth()->guard('admin');
		$auth->logout();
		return redirect('admin/login')->with(['success' => 'Logged Out']);
	}
}
