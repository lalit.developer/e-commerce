<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

class Dashboard extends Controller {

	protected $section;
	public function __construct(){
		$this->section = "Dashboard";
	}

	public function index() {
		$_data=array(
			'view'=>"list",
		);
		return view('admin/dashboard', $_data);
	}

}