<?php

function _price( $price = 0, $decimal = 2 ){
	$currency = '$';
	$price = str_replace( ',', '', $price );
	return $currency.number_format( round( $price, $decimal ), $decimal );
}

function _nice_date($bad_date = '', $format = FALSE){
	if(empty($bad_date)):
		$bad_date = date('Y-m-d');
	endif;
	if(!$format):
		$format = 'd, M, Y';
	endif;
	return Carbon::parse($bad_date)->format($format);
}

function _nice_time($bad_time = '', $timeformat = FALSE){
	if(empty($bad_time)):
		$bad_time = date('H:i');
	endif;
	if(!$timeformat):
		$timeformat = 'g:i A';
	endif;
	return Carbon::parse($bad_time)->format($timeformat);
}

function _preg_replace($url = ''){
	return preg_replace('/(\/+)/','/',$url);
	//return $url;
}