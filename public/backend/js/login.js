$(function() {
    cp.init();
});
cp = {
	notifyWithtEle: function(msg,type,pos="",timeout="") {
        var noty = new Noty({
            theme:'metroui',
            text: msg,
            type: type,
            layout: (pos != "") ? pos : 'topRight',
            timeout: (timeout != "") ? timeout : 1500,
            closeWith: ['click'],
            animation: {
                open: 'animated-slow fadeInRight',
                close: 'animated-slow fadeOutRight'
            }
        });
        noty.show();
    },
    init: function () {
        $('#loginform').validate({
        	rules: {
                email: {
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                },
            },
            messages: {
            	email: {
                    required: "Please enter email.",
                    email: "Please enter valid email."
                },
                password:{
                    required: "Please enter password.",
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
			submitHandler: function(form){
				var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
			}
		});
    }
};